class Elevator:
    """ Simple elevator class """
    # Переменная класса. Сколько людей было перевезено ВСЕМИ лифтами
    people_lifted = 0

    # Конструктор класса. Вызывается при создании экземпляра класса
    def __init__(self,name):
        self.name = name
        # переменная класса. Количество людей перевезенных КОНКРЕТНЫМ лифтом
        self.people_lifted = 0

    # Метод перевозки людей
    def lift(self):
        print ("{} lifted someone".format(self.name))
        # Увеличиваем количество людей перевезенных ЭТИМ лифтом
        self.people_lifted += 1
        # Увеличиваем количество людей перевезенных ВСЕМИ лифтами
        Elevator.people_lifted += 1

    # Метод печатающий информацию о конкретном лифте
    def info(self):
        print (self.name, "lifted", self.people_lifted, "people out of", Elevator.people_lifted)

# создание экземпляров класса
elevator_1 = Elevator("OTIS")
elevator_2 = Elevator("PHILLIPS")

# доступ к методам и полям классов
# Везем человека в лифте под именем OTIS
elevator_1.lift()
# Везем двоих человек в лифте под именем PHILLIPS
elevator_2.lift()
elevator_2.lift()
# Получаем информацию по лифту под именем OTIS
elevator_1.info()
# Получаем информацию по лифту под именем PHILLIPS
elevator_2.info()


# ==== Наследование ====
class Table:
    def __init__(self, l, w, h):
        self.length = l
        self.width = w
        self.height = h

class KitchenTable(Table):
    def setPlaces(self, p):
        self.places = p

class DeskTable(Table):
    def square(self):
        return self.width * self.length

# при создании экземпляров указывать значения полей в скобках обязательно
t1 = KitchenTable(2, 2, 0.7)
t2 = DeskTable(1.5, 0.8, 0.75)
t3 = KitchenTable(1, 1.2, 0.8)

# новый класс, дочерний от DeskTable у которого есть переопределение метода square()
class ComputerTable(DeskTable):
    def square(self, e):
        return self.width * self.length - e

ct = ComputerTable(2, 1, 1)
ct.square(0.3)

# расширение метода square с помощью расширения метода родительского класса
class NewComputerTable(DeskTable):
    def square(self, e):
        return DeskTable.square(self) - e

# создание дочернего класса с собственным конструктором
class NewKitchenTable(Table):
    def __init__(self, l, w, h, p):
        self.length = l
        self.width = w
        self.height = h
        self.places = p

# в примере выше почти весь код продублирован от родительского класса, поэтому
# оптимальней сделать еще раз расширение
class NewKitchenTable2(Table):
    def __init__(self, l, w, h, p):
        Table.__init__(self, l, w, h)
        self.places = p

tk = NewKitchenTable2(2, 1.5, 0.7, 10)
tk.places # выведет 10
tk.width  # выведет 1.5


# ==== Полиморфизм ====

# Случай 1. Разные не связанные классы имеют метод с одинаковым именем и различной реализацией
class T1:
     n=10
     def total(self, N):
          self.total = int(self.n) + int(N)

class T2:
     def total(self,s):
          self.total = len(str(s))

t1 = T1()
t2 = T2()
t1.total(45)
t2.total(45)
print(t1.total) # Вывод: 55
print(t2.total) # Вывод: 2

# Случай 2. Переопределение системного метода класса (в данном случае __str__() который ответственне за вывод данных в функцию print)
# оригинальный код
class A:
    def __init__(self, v1, v2):
        self.field1 = v1
        self.field2 = v2

a = A(3, 4)
print(a) #<__main__.A object at 0x7f840c8acfd0>

# переопределение метода __str__()
class A:
    def __init__(self, v1, v2):
        self.field1 = v1
        self.field2 = v2
    def __str__(self):
        return str(self.field1) + " " + str(self.field2)

a = A(3, 4)
print(a) # выведет 3 4

# таким способом можно построить квадратики
class Rectangle:
    def __init__(self, width, height, sign):
        self.w = int(width)
        self.h = int(height)
        self.s = str(sign)
    def __str__(self):
        rect = []
        for i in range(self.h): # количество строк
            rect.append(self.s * self.w) # знак повторяется w раз
        rect = '\n'.join(rect) # превращаем список в строку
        return rect

b = Rectangle(10, 3, '*')
print(b) # Вывод:
"""
**********
**********
**********
"""


# ==== Инкапсуляция ====

class B:
    count = 0
    def __init__(self):
        B.count += 1
    def __del__(self):
        B.count -= 1

a = B()
b = B()
print(B.count) # выведет 2
del a
print(B.count) # выведет 1

# такое присвоение все испортит
B.count -= 1
print(B.count) # будет выведен 0, хотя остался объект b


# для сокрытия поля класса используется соглашение - __ перед именем поля

class B:
    __count = 0
    def __init__(self):
        B.__count += 1
    def __del__(self):
        B.__count -= 1

a = B()
# выдаст ошибку:
# print(B.__count)
# однако сокрытие поля не настоящее и доступ к нему все же можно получить
print(B._B__count)

# геттеры для таких инкапсулированных полей вызываются через имя класса, а не экземпляра
class B:
    __count = 0
    def __init__(self):
        B.__count += 1
    def __del__(self):
        B.__count -= 1
    def qtyObject():
        return B.__count

a = B()
b = B()
print(B.qtyObject()) # будет выведено 2

# методы также делаются приватными через двойное подчеркивание перед именем
class DoubleList:
    def __init__(self, l):
        self.double = DoubleList.__makeDouble(l)
    def __makeDouble(old):
        new = []
        for i in old:
            new.append(i)
            new.append(i)
        return new

nums = DoubleList([1, 3, 4, 6, 12])
print(nums.double)
# print(DoubleList.__makeDouble([1,2])) # выдаст ошибку, потому что метод приватный


# ==== Композиция ====
# это создание одного класса из разных частей - т.е. других классов
# у нас есть комната, в ней окна и двери. Комнату надо обклеить обоями, а площадь дверей и окон вычесть
class Win_Door:
    def __init__(self, x, y):
        self.square = x * y

class Room:
    def __init__(self, x, y, z):
        self.square = 2 * z * (x + y)
        self.wd = []

    def addWD(self, w, h):
        self.wd.append(WinDoor(w, h))

    def workSurface(self):
        new_square = self.square
        for i in self.wd:
            new_square -= i.square
        return new_square

r1 = Room(6, 3, 2.7)
print(r1.square) # выведет 48.6
r1.addWD(1, 1)
r1.addWD(1, 1)
r1.addWD(1, 2)
print(r1.workSurface()) # выведет 44.6
