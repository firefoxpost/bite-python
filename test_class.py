class Tester:
    def __init__(self, name):
        self.name = name

    def makeTest(self):
        print(self.name, 'Start testing!')
        i = 4
        while i > 0:
            print('Making test N', i)
            i -= 1
        print(self.name, 'End testing!')


class Unit_Tester(Tester):
    def __init__(self, name):
        self.name = name

    def makeTest(self):
        super(Unit_Tester, self).makeTest()
        print(self.name, "Make special Unit Test!")
        print(self.name, "Unit Testing is finished!")


tester_1 = Tester('Ivan')
tester_1.makeTest()

tester_2 = Unit_Tester('Andrey')
tester_2.makeTest()
