import sys
import os

print('Аргументы командной строки')

for i in sys.argv:
    print(i)
    print('\n\nПеременная PYTHONPATH содержит', sys.path, '\n')

# получить путь текущего каталога
print(os.getcwd())