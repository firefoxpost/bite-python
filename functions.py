'''
def printMax(a, b):
    if a > b:
        print(a, 'максимально')
    elif a == b:
        print(a, 'равно', b)
    else:
        print(b, 'максимально')

printMax(3, 4) # прямая передача значений

x = 5
y = 7

printMax(x, y) # передача переменных в качестве аргументов
'''

'''
x = 50

def myFunc():
    global x
    print('x равен', x)
    x = 2
    print('Заменяем глобальное значение х на', x)

myFunc()
print('Значение x составляет', x)
'''

'''
def func_outer():
    x = 2
    print('x равно', x)

    def func_inner():
        x = 5

    func_inner()
    print('Локальное x сменилось на', x)

func_outer()
'''

'''
def say(message, times=1):
    print(message * times)

say('Привет')
say('Мир', 5)
'''

'''
def func(a, b=5, c=10):
    print('a равно', a, ', b равно', b, ', а c равно', c)

func(3, 7)
func(25, c=24)
func(c=50, a=100)
'''

def printMax(x, y):
    '''Выводит максимальное из двух чисел.

       Оба значения должны быть целыми числами.'''
    x = int(x) # конвертируем в целые, если возможно
    y = int(y)
    if x > y:
        print(x, 'наибольшее')
    else:
        print(y, 'наибольшее')

printMax(3, 5)
print(printMax.__doc__)