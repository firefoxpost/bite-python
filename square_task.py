import math

""" Задание:

Практическая работа. Приведенная ниже программа имеет ряд недочетов и недоработок. Требуется исправить и доработать, согласно следующему плану.

1. При вычислении оклеиваемой поверхности мы не "портим" поле self.square. В нем так и остается полная площадь стен. Ведь она может понадобиться,
если состав списка wd изменится, и придется заново вычислять оклеиваемую площадь.

2. Однако в классе не предусмотрено сохранение длин сторон, хотя они тоже могут понадобиться. Например, если потребуется изменить одну из величин у уже существующего объекта.
Площадь же помещения всегда можно вычислить, если хранить исходные параметры. Поэтому сохранять саму площадь в поле не обязательно.

3. Исправьте код так, чтобы у объектов Room были только четыре поля – width, length, height и wd.
Площади (полная и оклеиваемая) должны вычислять лишь при необходимости путем вызова методов.

4. Программа вычисляет площадь под оклейку, но ничего не говорит о том, сколько потребуется рулонов обоев. Добавьте метод, который принимает в качестве аргументов
длину и ширину одного рулона, а возвращает количество необходимых, исходя из оклеиваемой площади.

5. Разработайте интерфейс программы. Пусть она запрашивает у пользователя данные и выдает ему площадь оклеиваемой поверхности и количество необходимых рулонов.
"""

# ==== Композиция ====
# это создание одного класса из разных частей - т.е. других классов
# у нас есть комната, в ней окна и двери. Комнату надо обклеить обоями, а площадь дверей и окон вычесть
class Win_Door:
    def __init__(self):
        self.x = float(input('Enter win/door width:'))
        self.y = float(input('Enter win/door height:'))

    def countSquare(self):
        return self.x * self.y

class Wall_Paper:
    def __init__(self):
        self.x = float(input('Enter wallpaper width:'))
        self.y = float(input('Enter wallpaper height:'))

    def countSquare(self):
        return self.x * self.y

class Room:
    def __init__(self):
        self.x = float(input('Enter room width:'))
        self.y = float(input('Enter room length:'))
        self.z = float(input('Enter room height:'))
        self.wd = []

    def addWD(self):
        self.wd.append(Win_Door())

    def countSquare(self):
        return 2 * self.z * (self.x + self.y)

    def workSurface(self):
        new_square = self.countSquare()
        for i in self.wd:
            new_square -= i.countSquare()
        return new_square

    def countWallpapers(self):
        wall_paper = Wall_Paper()
        return math.ceil(self.workSurface()/wall_paper.countSquare())

r1 = Room() # 6, 3, 2.7
print("Room total square:", r1.countSquare()) # выведет 48.6
r1.addWD() # 1, 1
r1.addWD() # 1, 1
r1.addWD() # 1, 2
print("Working surface:", r1.workSurface()) # выведет 44.6
print("Wallpapers quantity:", r1.countWallpapers()) # 20, 1.5 # выведет 2
